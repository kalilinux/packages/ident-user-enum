Source: ident-user-enum
Section: utils
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Ben Wilson <g0tmi1k@kali.org>,
           Sophie Brun <sophie@offensive-security.com>,
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.2
Homepage: https://pentestmonkey.net/tools/user-enumeration/ident-user-enum
Vcs-Git: https://gitlab.com/kalilinux/packages/ident-user-enum.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/ident-user-enum

Package: ident-user-enum
Architecture: all
Depends: ${misc:Depends}, perl, libnet-ident-perl, libio-socket-ip-perl
Description: Query ident to determine the owner of a TCP network process
 This package is a simple PERL script to query the ident service (113/TCP)
 in order to determine the owner of the process listening on each TCP port of
 a target system.
 .
 This can help to prioritise target service during a pentest (you might want
 to attack services running as root first).
 Alternatively, the list of usernames gathered can be used for password
 guessing attacks on other network services.
